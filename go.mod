module github.com/foo-dogsquared/hugo-theme-more-contentful

go 1.15

require (
	github.com/foo-dogsquared/hugo-mod-simple-icons v0.0.0-20201026173406-633714bb359d // indirect
	github.com/foo-dogsquared/hugo-theme-contentful v1.2.1-0.20201030114530-c0a2488a58fb // indirect
	github.com/refactoringui/heroicons v0.4.2 // indirect
)
